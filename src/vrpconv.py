#!/usr/bin/python3

import sys
import os
import argparse
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../intern')
import utilyq as uq
import rinstyq as rq

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Convert from a vrp format')
	parser.add_argument('vrpfn', nargs='?', type=str, default=None, help='the vrp instance filename. If empty, the program will read data from stdin in a non blocking way (this only works in Linux systems)')
	parser.add_argument('k', type=int, help='number of vehicles. If 0, will get the k from the vrpfn content')
	parser.add_argument('-l','--real_dist', dest='real_dist', action='store_true', default=False, help='use real distance measure')
	parser.add_argument('-d','--num_dec_places_real_dist', dest='num_dec_places_real_dist', type=int, default=0, help='')
	parser.add_argument('-f','--outfmt', type=str, default='dat', choices=['dat','yaml'], help='output format. Default dat (suitable to be used by CPLEX OPL)')
	parser.add_argument('-v','--verb', action='store_true', help='Very verbose mode for debug purposes')
	parser.add_argument('-m','--msg', action='store_true', help='Show informative messages')

	params = parser.parse_args()

	uq.verb, uq.msg = uq.setup_msgs(params.verb, params.msg)

	if params.k < 0:
		sys.exit('Error: main: k == ' + str(params.k) + ' < 0')

	if not params.real_dist:
		if params.num_dec_places_real_dist != 0:
			uq.fail('Error: main: real_dist == ' + str(params.real_dist) + ' and num_dec_places_real_dist ' + str(params.num_dec_places_real_dist) + ' != 0')
	else:
		if params.num_dec_places_real_dist < 0:
			uq.fail('Error: main: real_dist == ' + str(params.real_dist) + ' and num_dec_places_real_dist ' + str(params.num_dec_places_real_dist) + ' < 0')

	if params.vrpfn and not sys.stdin.isatty():
		uq.fail('Error: main: there are input vrp instance filename and data in stdin (user must supply exactly one)')
	elif params.vrpfn:
		file = open(params.vrpfn, 'r')
	elif not sys.stdin.isatty():
		file = sys.stdin
	else:
		uq.fail('Error: main: there is no input vrp filename nor stdin data')

	inst = rq.Inst()
	inst.real_dist_measure        = params.real_dist
	inst.num_dec_places_real_dist = params.num_dec_places_real_dist
	inst.recgn(file.read().splitlines())

	if not inst.inst_id and params.vrpfn:
		inst.inst_id = os.path.splitext(os.path.basename(params.vrpfn))[0]

	inst.calc_dists()
	inst.calc_intern_data()

	inst.calc_and_check_extern_data(extern_k=params.k)

	inst.fill_discrete()

	if params.outfmt == 'dat':
		print(inst.as_dat_str())
	else:
		uq.fail('Error: main')
#		print(vrpinst.as_yaml_str())

	if params.vrpfn:
		file.close()

