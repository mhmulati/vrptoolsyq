#!/usr/bin/env -S python3 -B
import sys
import os
import argparse
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../intern')
import utilyq as uq
import rinstyq as rq

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Show vrp instance info, including the value of the canonical route given by: 0, 1, ..., n - 1')
	parser.add_argument('vrpfn', nargs='?', type=str, default=None, help='the vrp instance filename. If empty, the program will read data from stdin in a non blocking way (this only works in Linux systems)')
	parser.add_argument('-k', '--vehicles', type=int, default=0, help='number of vehicles. Default 0. If 0, will get the k from the vrpfn content')
	parser.add_argument('-R', '--rho', type=float, default=0.0)
	parser.add_argument('-l','--real_dist', dest='real_dist', action='store_true', default=False, help='use real distance measure')
	parser.add_argument('-d','--num_dec_places_real_dist', dest='num_dec_places_real_dist', type=int, default=0, help='')
	parser.add_argument('-G', '--gcd_ccd', type=int, default=1)
	parser.add_argument('-v','--verb', action='store_true', help='Very verbose mode for debug purposes')
	parser.add_argument('-m','--msg', action='store_true', help='Show informative messages')

	params = parser.parse_args()

	uq.verb, uq.msg = uq.setup_msgs(params.verb, params.msg)

	if not params.real_dist:
		if params.num_dec_places_real_dist != 0:
			uq.fail('Error: main: real_dist == ' + str(params.real_dist) + ' and num_dec_places_real_dist ' + str(params.num_dec_places_real_dist) + ' != 0')
	else:
		if params.num_dec_places_real_dist < 0:
			uq.fail('Error: main: real_dist == ' + str(params.real_dist) + ' and num_dec_places_real_dist ' + str(params.num_dec_places_real_dist) + ' < 0')

	if params.vrpfn and not sys.stdin.isatty():
		uq.fail('Error: main: there are input vrp instance filename and data in stdin (user must supply exactly one)')
	elif params.vrpfn:
		instfile = open(params.vrpfn, 'r')
	elif not sys.stdin.isatty():
		instfile = sys.stdin
	else:
		uq.fail('Error: main: there is no input vrp filename nor stdin data')

	extern_k = params.vehicles
	extern_rho = params.rho
	extern_q0 = 0.0
	override_inst_k = True
	override_inst_q0 = True
	real_dist = params.real_dist
	num_dec_places_real_dist = params.num_dec_places_real_dist
	gcd_ccd = params.gcd_ccd

	inst = rq.Inst(instfile,
		extern_k,
		extern_rho,
		extern_q0,
		override_inst_k,
		override_inst_q0,
		real_dist,
		num_dec_places_real_dist,
		0.0,
		0.0,
		False,
		gcd_ccd)

	instfile.close()

	if not inst.inst_id and params.vrpfn:
		inst.inst_id = os.path.splitext(os.path.basename(params.vrpfn))[0]

	uq.log_(inst.as_str())
